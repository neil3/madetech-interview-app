class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email, null: false, default: '', limit: 150
      t.string :username, null: false, default: '', limit: 20
      t.string :encrypted_password, null: false, default: '', limit: 40
      t.string :salt, null: false, default: '', limit: 40
      t.timestamps null: false
    end
  end
end
