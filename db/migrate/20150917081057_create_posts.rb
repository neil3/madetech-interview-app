class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.references :user
      t.string :body, null: false, default: '', limit: 150
      t.timestamps null: false
    end
  end
end
