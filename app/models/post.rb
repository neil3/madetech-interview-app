class Post < ActiveRecord::Base
  belongs_to :user

  validates :user, presence: true
  validates :body, presence:true, length: { maximum: 150 }

  scope :ordered_newest_oldest, -> { order('created_at DESC') }

end
