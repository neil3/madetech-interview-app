class User < ActiveRecord::Base
  validates :email, presence: true, email: true, length: { maximum: 150 }
  validates :username, presence: true, uniqueness: true, length: { maximum: 20 }
  validates :encrypted_password, presence: true, length: { is: 40 }
  validates :salt, presence: true, length: { maximum: 40 }

  has_many :posts, dependent: :destroy
end
