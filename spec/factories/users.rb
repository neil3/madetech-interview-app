FactoryGirl.define do
  factory :user do
    email "neil@neilturner.me"
    username "neilturner77"
    encrypted_password "1111111111222222222233333333334444444444"
    salt "test"
  end

end
