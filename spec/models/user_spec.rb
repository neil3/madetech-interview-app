require 'rails_helper'

RSpec.describe User, type: :model do
  it { should validate_presence_of(:email) }
  it { should validate_length_of(:email).is_at_most(150) }
  it { should validate_presence_of(:username) }
  it { should validate_length_of(:username).is_at_most(20) }
  it { should validate_uniqueness_of(:username) }
  it { should validate_presence_of(:encrypted_password) }
  it { should validate_length_of(:encrypted_password).is_equal_to(40) }
  it { should validate_presence_of(:salt) }
  it { should validate_length_of(:salt).is_at_most(40) }
end
