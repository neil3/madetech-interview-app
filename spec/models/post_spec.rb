require 'rails_helper'

RSpec.describe Post, type: :model do
  it { should validate_presence_of(:user) }
  it { should validate_presence_of(:body) }
  it { should validate_length_of(:body).is_at_most(150) }

  it "orders by newest to oldest" do
    user = FactoryGirl.create(:user)
    post_1 = FactoryGirl.create(:post, user: user)
    sleep 1
    post_2 = FactoryGirl.create(:post, user: user)
    expect(Post.ordered_newest_oldest).to eq([post_2, post_1])
  end
end
